main(List<String> args) {
  final x = args.isNotEmpty ? int.parse(args[0]) : 0;

  final map = <String, dynamic>{
    'key1': 12,
    'key2': Object(),
  };

  final set = <int>{1, 3};
  final set2 = {1, 3};

  final list = <String>['1', '2', '3'];
  final list2 = [if (x > 0) 1, 2, 3];

  if (args.isNotEmpty) {
    final y1 = args.first ?? 0;
    final y2 = set.first ?? 2.0;
  }

  final list3 = [
    1,
    2,
    for (var item in list) int.parse(item),
    4,
  ];

  list3.forEach(print);
}
