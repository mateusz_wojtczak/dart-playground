typedef Fun = int Function(String x);

void main() {
  final f = (x) {
    print('what');
  };

  foo((x) {
    print('what');
  });

  foo(f);

  // foo(someFunction);
}

void foo(Fun f) {
  print(1 + f('dart'));
}

void someFunction(String x) {
  print('not now');
}
