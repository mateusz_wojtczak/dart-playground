class Point {
  const Point(this.x, this.y);

  final double x, y;

  Point operator +(Point p) => Point(x + p.x, y + p.y);

  @override
  String toString() {
    return '($x, $y)';
  }
}

mixin Sum on Point {
  double get sum => x + y;
}

class MyPoint extends Point with Sum {
  MyPoint(double x, double y) : super(x, y);
}

class MyPoint2 = Point with Sum;

main(List<String> args) {
  final myPoint = MyPoint2(1, 2);

  print(myPoint.sum);

  print(myPoint + Point(3, 5));
}
