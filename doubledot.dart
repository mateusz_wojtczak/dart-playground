class Coworker {
  String firstName;
  String lastName;

  String get fullName => '$firstName $lastName';
  void set fullName(String x) {
    firstName = x.split(' ').first;
    lastName = x.split(' ').last;
  }

  void hire() => print('hired!');
  void fire() => print('fired!');
}

main(List<String> args) {
  final person = Coworker()
    ..firstName = 'Mateusz'
    ..lastName = 'Wojtczak';

  print(person.fullName);
  person.fullName = 'Jan Nowak';

  print(person.firstName);
  print(person.lastName);

  person
    ..hire()
    ..fire();
}
