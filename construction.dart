import 'dart:math';

class Consts {
  static double pi = 3.14;
}

class Point {
  //const Point(this.x, this.y) : diagonal2 = x*x + y*y;

  Point(this.x, this.y) : diagonal = sqrt(x * x + y * y);

  Point.withX(this.x)
      : y = 0,
        diagonal = 0;

  Point.optional(this.x, this.y, {this.diagonal});

  final double x;
  final double y;

  final double diagonal;
  // final double diagonal2;
}

main(List<String> args) {
  final a = Point(1, 2);
  final b = Point.withX(5);

  final c1 = Point.optional(1, 2);
  final c2 = Point.optional(1, 2, diagonal: sqrt(5));
}
