abstract class Logger {
  void log(String message);

  @override
  String toString() {
    return 'I\'m a logger';
  }
}

class FakeLogger extends Logger {
  @override
  void log(String message) {
    // do nothing
  }
}

class FakeLogger2 implements Logger {
  @override
  void log(String message) {
    // do nothing
  }

  void someFakeFunction() {}
}

class MyError extends Error implements Logger {
  @override
  void log(String message) {
    print(toString());
  }
}

class MyGenericLogger<T extends num> extends Logger implements FakeLogger2 {
  @override
  void log(String message) {
    // do nothing
  }

  void plus5(T x) {
    print(x + 5);
  }

  @override
  void someFakeFunction() {
    print('nope');
  }
}

main(List<String> args) {
  final myError = MyError();
  final sham = FakeLogger();
  final evenLargerSham = FakeLogger2();

  myError.log('smth');

  print(sham.toString());
  print(evenLargerSham.toString());

  print('myError is a ${myError is Logger}');
  print('sham is a ${sham is Logger}');
  print('evenLargerSham is a ${evenLargerSham is Logger}');

  print((myError as Logger).toString());

  throw myError;
}
